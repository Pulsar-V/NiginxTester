#include <stdio.h>
#include <pthread.h>
#include <malloc.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <zconf.h>
#include <netdb.h>

/**
 * 互斥锁
 */
pthread_mutex_t key;
/**
 * 计时器结构
 */
typedef struct _Vector{
    int length;
    double time;
}Vector,*pVector;

typedef struct _thread_arg{
    int id;
    int port;
    char *IPAddress;
}thread_arg,*pthread_arg;

double *run_time;
int count=0;
double all_task=0;

void process_http(int handel,char *target){
    int send_num;
    char send_buf [] = "helloworld";
    char recv_buf [4096];
    char str1[4096];
    char dst_url[1024];
    char *url="GET http://%s/ HTTP/1.1\r\n";
    sprintf(dst_url,url,target);
    char *dst_host[1024];
    char *host="Host: %s\r\n";
    sprintf(dst_host,host,target);
    while (1)
    {
        //printf("begin send\n");
        memset(str1,0,4096);
        strcat(str1, dst_url);
        strcat(str1,dst_host);
        strcat(str1,"Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\n");
        strcat(str1,"Accept-Language: en-GB,en;q=0.5\r\n");
        strcat(str1,"Content-Type: application/x-www-form-urlencoded\r\n");
        strcat(str1,"\r\n");
        strcat(str1,"\r\n\r\n");
        //printf("str1 = %s\n",str1);

        send_num = send(handel, str1,strlen(str1),0);
        if (send_num < 0)
        {
            perror("send error");
            exit(1);
        }
        else
        {

            //printf("send successful\n");
            //printf("begin recv:\n");
            int recv_num = recv(handel,recv_buf,sizeof(recv_buf),0);
            if(recv_num < 0){
                perror("recv");
                exit(1);
            } else {
                //printf("recv sucess:%s\n",recv_buf);
                //printf("recv sucess\n");
            }
        }
        break;
    }
}

void *send_http_socket(void *arg){
    thread_arg *argument=(pthread_arg)arg;
    clock_t start, finish;
    start = clock();
    int id=argument->id;
    int socket_fd;
    struct sockaddr_in addr_serv;
    socket_fd=socket(AF_INET,SOCK_STREAM,NULL);
    if (socket_fd<0) {
        perror("sock error");
        exit(0);
    } else{
        //printf("sock successful\n");
    }
//    struct hostent *hostInfo= gethostbyaddr(IPAddress,4,AF_INET);
    struct hostent *hostInfo= gethostbyname(argument->IPAddress);
    if(hostInfo==0){
        perror("host info is null");
        exit(-6);
    }
    memset(&addr_serv, 0, sizeof(addr_serv));
    addr_serv.sin_family = AF_INET;
    addr_serv.sin_port = htons(argument->port);

    //printf("Ip address = %s \n",inet_ntoa(*((struct in_addr*)hostInfo->h_addr)));
    memcpy(&addr_serv.sin_addr, &(*hostInfo->h_addr_list[0]), hostInfo->h_length);

    if (connect(socket_fd, (struct sockaddr*)(&addr_serv), sizeof(addr_serv)) < 0)
    {
        perror("connect error\n");
        exit(1);
    }
    else
    {
        //printf("connect successful\n");
    }
    process_http(socket_fd,argument->IPAddress);
    finish = clock();
    pthread_mutex_lock(&key);
    double rtime=(double)(finish-start) / CLOCKS_PER_SEC;
    count+=1;
    printf("Task:%f%\n",((double)count/all_task)*100);
    run_time[id]=rtime;
    pthread_mutex_unlock(&key);
    return ((void*)0);
}

void create_http_tester(int num,int port,char *IPAddress){
    pthread_t *last_thread;
    for (int i=0;i<num;i+=1){
        pthread_t thread;
        thread_arg argument;
        char t_IPAddress[64];
        strcpy(t_IPAddress,IPAddress);
        argument.id=i;
        argument.port=port;
        argument.IPAddress=t_IPAddress;
        pthread_create(&thread,NULL,&send_http_socket,&argument);
        usleep(100);
        last_thread=&thread;
    }
        pthread_join(*last_thread,NULL);
}

int main() {
//    请求线程数 端口 IP地址
//    int n,port;char IPAddress[64];
    pthread_mutex_init(&key,NULL);
    int n=100,port=80;
    all_task=(double)n;
    char *IPAddress="download.savannah.gnu.org";
    printf("Input Request Number Port and Address\n");
//    scanf("%d %d %s",&n,&port,IPAddress);
//    申请计时器结构空间
    run_time=(double*)malloc(sizeof(run_time)*n);
    create_http_tester(n,port,IPAddress);
    while(count<n){};
    double sum=0;
    for(int i=0;i<n;i+=1){
        sum+=run_time[i];
        printf("%f ",run_time[i]);
    }
    printf("\n");
    printf("平均用时:%f\n",sum/n);
    return 0;
}